dockmon
========

Готовое решение для мониторинга хост-машины и контейнеров [Prometheus](https://prometheus.io/), [Grafana](http://grafana.org/), [cAdvisor](https://github.com/google/cadvisor),
[NodeExporter](https://github.com/prometheus/node_exporter), [AlertManager](https://github.com/prometheus/alertmanager).

## Установка

Скопируйте репозиторий на свою хост-машину, зайдите в папку dockmon и запустите контейнер:

```bash
git clone https://bitbucket.org/citronium/dockmon/
cd dockmon

ADMIN_USER=admin ADMIN_PASSWORD=admin docker-compose up -d
```
Или перейдите на http://<host-ip>:3000 и авторизуйтесь, используя логин admin и пароль changeme. Вы можете изменить пароль с помощью Grafana UI или изменив файл user.config.

Требования:

* Docker Engine >= 1.13
* Docker Compose >= 1.11

Контейнеры:

* Prometheus (metrics database) `http://<host-ip>:9090`
* Prometheus-Pushgateway (push acceptor for ephemeral and batch jobs) `http://<host-ip>:9091`
* AlertManager (alerts management) `http://<host-ip>:9093`
* Grafana (visualize metrics) `http://<host-ip>:3000`
* NodeExporter (host metrics collector)
* cAdvisor (containers metrics collector)
* Caddy (reverse proxy and basic auth provider for prometheus and alertmanager)

## Установка Grafana локально

Перейдите по адресу `http://<host-ip>:3000` и авторизуйтесь с логином ***admin*** и паролем ***admin***. Вы можете изменить данные для авторизации в compose файле или добавив переменные окружения`ADMIN_USER` и `ADMIN_PASSWORD` или при раскручивании композа. Файл конфигурации может быть добавлен в композ файл и следующим образом:
```
grafana:
  image: grafana/grafana:5.2.4
  env_file:
    - config

```
и файл с конфигурацией должен быть следующего вида (лежит в корне проекта)
```
GF_SECURITY_ADMIN_USER=admin
GF_SECURITY_ADMIN_PASSWORD=changeme
GF_USERS_ALLOW_SIGN_UP=false
```
Если вы хотите сменить пароль, то вам нужно будет удалить следующую запись
```
- grafana_data:/var/lib/grafana
```

## Установка Grafana при использовании Traefik

Для работы с Grafana при использовании реверс-прокси Traefik в композ файл нужно добавить внешнюю сеть Traefik, навесить лэйблы на Grafana и добавить Grafana в сеть Traefik:
```
networks:
  monitor-net:
    driver: bridge
  traefik:
    external:
      name: traefik_proxy
```
Добавление сети и лэйблов:
```
networks:
      - traefik
      - monitor-net
    labels:
      - traefik.docker.network=traefik_proxy
      - traefik.enable=true
      - traefik.backend=grafana
      - traefik.frontend.rule=Host:{your_Grafana_url}
      - traefik.port=3000


```


Из меню Grafana выберите пункт «Источники данных» (Data Sources) и кликните «Добавить источник данных» (Add Data Source). Чтобы добавить контейнеры Prometheus как источник данных, используйте следующие значения:

* Name: Prometheus
* Type: Prometheus
* Url: http://prometheus:9090
* Access: proxy

***Панель управления сервера***

![Grafana_Docker_Host](https://bitbucket.org/citronium/dockmon/raw/7592a4bd34191a8738d019f7802417554dce902f/screens/Grafana_Docker_Host.png)

Дашборд Docker Host отображает ключевые метрики для мониторинга использования ресурсов сервера:

* Аптайм сервера, процент простоя CPU, количество ядер CPU, доступнуая память, swap и хранилище.
* График средней нагрузки системы, график выполненных и заблокированных IO-процессов, график прерываний.
* График использования CPU в режимах guest, idle, iowait, irq, nice, softirq, steal, system, user.
* График использования памяти по распределению (использовано, свободно, буферы, кэшировано).
* График использования IO (read Bps, read Bps and IO time).
* График использования сети устройствами (входящий Bps, исходящий Bps)
* Использование Swap и графики активности

Для хранилища и особенно для графика свободного места, вам необходимо указать тип файловой системы в реквесте. Вы можете найти его по следующему адресу `grafana/dashboards/docker_host.json`, на строке 480 :

      "expr": "sum(node_filesystem_free_bytes{fstype=\"ext4\"})",

Я работаю на ext4, поэтому прописано у меня изначально `ext4`.

Вы можете найти нужное значение для своей системы при помощи Prometheus `http://<host-ip>:9090` выполнив следующий запрос :

      node_filesystem_free_bytes

***Панель управления контейнерами докера***

![Grafana_Docker_Containers](https://bitbucket.org/citronium/dockmon/raw/7592a4bd34191a8738d019f7802417554dce902f/screens/Grafana_Docker_Containers.png)

Панель управления контейнеров докера отображает ключевые метрики для мониторинга используемых контейнеров:

* Общая нагрузка контейнеров ЦПУ, использование памяти и хранилища.
* График используемых контейнеров, график нагрузки системы, график использования IO.
* График использования контейнера ЦПУ.
* График использования памяти контейнера.
* График использования кэшированной памяти.
* График входящего использования сети контейнеров.
* График исходящего использования сети контейнеров.

На панели не представлены контейнеры, являющиеся частью стека мониторинга.

***Панель управления мониторинговыми сервисами***

![Grafana_Prometheus](https://bitbucket.org/citronium/dockmon/raw/7592a4bd34191a8738d019f7802417554dce902f/screens/Grafana_Prometheus.png)

Панель управления мониторинговыми сервисами отображает ключевые метрики для мониторинга контейнеров, составляющих мониторинговый стек:

* Время работы контейнера Prometheus, общее использование памяти мониторингового стека, фрагменты и серии памяти локального хранилища Prometheus.
* График использования контейнера ЦПУ.
* График использования памяти контейнера.
* Графики сохраняемых фрагментов Prometheus и срочности сохранения.
* Графики операций фрагментов Prometheus и продолжительности установления контрольных точек.
* Графики процента использованных шаблонов Prometheus, целевых считываний и продолжительности считывания.
* График запросов Prometheus HTTP.
* График уведомлений Prometheus.

Я настроил период хранения данных в Prometheus до 200 часов и размер базы 1Gb, эти переменные задаются в compose файле.

```yaml
  prometheus:
    image: prom/prometheus
    command:
      - '-storage.local.target-heap-size=1073741824'
      - '-storage.local.retention=200h'
```

## Определение уведомлений

Я установил три уровня конфигурации уведомлений:

* Уведомления сервисов мониторинга targets.rules;
* Уведомления хоста докера hosts.rules;
* Уведомления контейнеров докера containers.rules.

Вы можете изменять правила уведомления и перезагружать их с помощью запроса HTTP POST:

```
curl -X POST http://admin:admin@<host-ip>:9090/-/reload
```

***Уведомления сервисов мониторинга***

Если один из целевых объектов (node-exporter и cAdvisor) не отвечает более 30 секунд, включите уведомление:

```yaml
ALERT monitor_service_down
  IF up == 0
  FOR 30s
  LABELS { severity = "critical" }
  ANNOTATIONS {
      summary = "Monitor service non-operational",
      description = "{{ $labels.instance }} service is down.",
  }
```

***Уведомление хоста докера***

Если CPU хоста докера находится под высокой нагрузкой более 30 секунд, включите уведомление:

```yaml
ALERT high_cpu_load
  IF node_load1 > 1.5
  FOR 30s
  LABELS { severity = "warning" }
  ANNOTATIONS {
      summary = "Server under high load",
      description = "Docker host is under high load, the avg load 1m is at {{ $value}}. Reported by instance {{ $labels.instance }} of job {{ $labels.job }}.",
  }
```

Измените пороговое значение нагрузки в соответствии с количеством ядер CPU.

Если память хоста докера заполнена, включите уведомление:

```yaml
ALERT high_memory_load
  IF (sum(node_memory_MemTotal_bytes) - sum(node_memory_MemFree_bytes + node_memory_Buffers_bytes + node_memory_Cached_bytes) ) / sum(node_memory_MemTotal_bytes) * 100 > 85
  FOR 30s
  LABELS { severity = "warning" }
  ANNOTATIONS {
      summary = "Server memory is almost full",
      description = "Docker host memory usage is {{ humanize $value}}%. Reported by instance {{ $labels.instance }} of job {{ $labels.job }}.",
  }
```

Если хранилище хоста докера заполнено, включите уведомление:

```yaml
ALERT high_storage_load
  IF (node_filesystem_size_bytes{fstype="aufs"} - node_filesystem_free_bytes{fstype="aufs"}) / node_filesystem_size_bytes{fstype="aufs"}  * 100 > 85
  FOR 30s
  LABELS { severity = "warning" }
  ANNOTATIONS {
      summary = "Server storage is almost full",
      description = "Docker host storage usage is {{ humanize $value}}%. Reported by instance {{ $labels.instance }} of job {{ $labels.job }}.",
  }
```

***Уведомления контейнеров докера***

Если контейнер не отвечает в течение 30 секунд, включите уведомление

```yaml
ALERT jenkins_down
  IF absent(container_memory_usage_bytes{name="jenkins"})
  FOR 30s
  LABELS { severity = "critical" }
  ANNOTATIONS {
    summary= "Jenkins down",
    description= "Jenkins container is down for more than 30 seconds."
  }
```

Если контейнер использует более 10 % ядер CPU более 30 секунд, включите уведомление:

```yaml
 ALERT jenkins_high_cpu
  IF sum(rate(container_cpu_usage_seconds_total{name="jenkins"}[1m])) / count(node_cpu_seconds_total{mode="system"}) * 100 > 10
  FOR 30s
  LABELS { severity = "warning" }
  ANNOTATIONS {
    summary= "Jenkins high CPU usage",
    description= "Jenkins CPU usage is {{ humanize $value}}%."
  }
```

Если контейнер использует более 1,2 Гб RAM в течение 30 секунд, включите уведомление:

```yaml
ALERT jenkins_high_memory
  IF sum(container_memory_usage_bytes{name="jenkins"}) > 1200000000
  FOR 30s
  LABELS { severity = "warning" }
  ANNOTATIONS {
      summary = "Jenkins high memory usage",
      description = "Jenkins memory consumption is at {{ humanize $value}}.",
  }
```

## Настройка уведомлений

Сервис AlertManager отвечает за передачу уведомлений сервера Prometheus. AlertManager может посылать уведомления с помощью электронной почты, Pushover, Slack, HipChat и других систем, использующих интерфейс webhook.
Полный список интеграций можно найти  [здесь](https://prometheus.io/docs/alerting/configuration).

Здесь вы можете просмотреть или выключить уведомления `http://<host-ip>:9093`.

Получение уведомлений можно настроить в файле [alertmanager/config.yml](https://bitbucket.org/citronium/dockmon/src/master/alertmanager/config.yml).


Чтобы получать уведомления через Slack, необходимо настроить интеграцию, выбрав ***incoming web hooks*** на странице приложения. Больше деталей по интеграции со Slack можно найти  [здесь](http://www.robustperception.io/using-slack-with-the-alertmanager/).

Скопируйте Slack Webhook URL в поле ***api_url*** и определите канал Slack ***channel***.

```yaml
route:
    receiver: 'slack'

receivers:
    - name: 'slack'
      slack_configs:
          - send_resolved: true
            text: "{{ .CommonAnnotations.description }}"
            username: 'Prometheus'
            channel: '#<channel>'
            api_url: 'https://hooks.slack.com/services/<webhook-id>'
```

![picture](https://bitbucket.org/citronium/dockmon/raw/7592a4bd34191a8738d019f7802417554dce902f/screens/Grafana_Prometheus.png)

## Отправка метрик в Pushgateway

[Pushgateway](https://github.com/prometheus/pushgateway) используется для сбора метрик с кратковременных работ или других сервисов

Для пуша данных выполните следующую команду:

    echo "some_metric 3.14" | curl --data-binary @- http://user:password@localhost:9091/metrics/job/some_job

Замените `user:password` на те, что вы указали при начальной настройке (по умолчанию: `admin:admin`).
