#!/usr/bin/env bash

sudo curl -sSL https://get.docker.com/ | bash;
sudo usermod -aG docker $USER;

sudo curl -L https://github.com/docker/compose/releases/download/1.21.2/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose;
sudo chmod +x /usr/local/bin/docker-compose;

sudo apt -y install python;

sudo apt -y install mc;