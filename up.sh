#!/usr/bin/env bash

cd traefik;
docker-compose up -d;
cd ..;
cd portainer;
docker-compose up -d;
cd ..;
cd dockmon;
docker-compose up -d;
cd ..;